#ifndef PGM_HPP
#define PGM_HPP
#include <string>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstring>
#include "imagem.hpp"

using namespace std;
class Pgm : public Imagem{
private:
  int posicaoInicial;
public:
  //Construtor/Destrutor
  Pgm();
  Pgm(string endereco);
  ~Pgm();
  //Método de acesso get
  int getPosicaoInicial();
  //Métodos
  void pegarPosicaoInicial();//Método que pega a posição inicial da mensagem
  int calculaPosicaoInicial();//Método que calcula aposição inicial da imagem

  void testeNumeroMagico();//Método verifica se o número mágico é P5
protected:
  //Método de acesso set
  void setPosicaoInicial(int posicaoInicial);
};
#endif
