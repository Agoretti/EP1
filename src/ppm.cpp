#include "ppm.hpp"
using namespace std;
Ppm::Ppm(){
  setAltura(0);
  setLargura(0);
  setTamanhoTotal(0);
  setEndereco("  ");
  setNumeroMagico("  ");
}
Ppm::Ppm(string endereco){
  setAltura(0);
  setLargura(0);
  setTamanhoTotal(0);
  setEndereco(endereco);
  setNumeroMagico("  ");
}
Ppm::~Ppm(){

}
int Ppm::calculaPosicaoInicial(){
  int contador1=0,auxiliar;
  for(int contador=0;getPixel()->at(contador)&&contador1<3;contador++){
       if(getPixel()->at(contador)=='\n'){
         contador1++;
       }
       if(getPixel()->at(contador)=='#'){
         contador1--;
       }
       if (contador1==3){
         auxiliar=contador;
       }
     }
  return auxiliar+1;
}
void Ppm::testeNumeroMagico(){
  if (strcmp(getNumeroMagico().c_str(),"P6")){
    cout<<"Erro, arquivo não é Ppm!"<<endl;
    exit(0);
  }
}
