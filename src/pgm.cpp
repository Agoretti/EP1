#include "pgm.hpp"
using namespace std;

Pgm::Pgm(){
  setAltura(0);
  setLargura(0);
  setTamanhoTotal(0);
  setEndereco("  ");
  setNumeroMagico("  ");
  setPosicaoInicial(0);
}
Pgm::Pgm(string endereco){
  setAltura(0);
  setLargura(0);
  setTamanhoTotal(0);
  setEndereco(endereco);
  setNumeroMagico("  ");
  this->posicaoInicial=0;
}
Pgm::~Pgm(){
}
int Pgm::getPosicaoInicial(){
  return this->posicaoInicial;
}
void Pgm::setPosicaoInicial(int posicaoInicial){
  this->posicaoInicial=posicaoInicial;
}
void Pgm::pegarPosicaoInicial(){
  int contador=0;
  string posicaoInicial;
  for(int contador1=0;contador1<(int)getPixel()->size() && contador<2;contador1++){
    if (getPixel()->at(contador1)=='\n'){
      contador++;
    }
    if (getPixel()->at(contador1)=='#'){
      if(contador==1){
        for(int contador2=contador1+2;getPixel()->at(contador2)!=' ';contador2++){
          posicaoInicial+=getPixel()->at(contador2);
        }
      this->posicaoInicial=stoi(posicaoInicial);
      contador++;
      }
    }
  }
}
int Pgm::calculaPosicaoInicial(){
  int contador1=0,auxiliar;
  for(int contador=0;getPixel()->at(contador)&&contador1<3;contador++){
       if(getPixel()->at(contador)=='\n'){
         contador1++;
       }
       if(getPixel()->at(contador)=='#'){
         contador1--;
       }
       if (contador1==3){
         auxiliar=contador;
       }
     }
  return auxiliar+1+posicaoInicial;
}

void Pgm::testeNumeroMagico(){
  if (strcmp(getNumeroMagico().c_str(),"P5")){
    cout<<"Erro, arquivo não é Pgm!"<<endl;
    exit(0);
  }
}
